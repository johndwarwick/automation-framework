import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;

public class steps {
    private AdminLogonPOM adminPOM;
    private AdminHomePage adminHomePage;
    private HomePagePOM homePage;

    @Given("^I log on as admin user with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void iLogOnAsAdminUserWithUsernameAndPassword(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        adminPOM = PageFactory.initElements(WebDrv.getInstance().openBrowser("http://localhost:8000/admin/"), AdminLogonPOM.class);
        adminPOM.enterUsername(arg0);
        adminPOM.enterPassword(arg1);
        adminPOM.clickOnSignIn();
    }

    @When("^I create a page$")
    public void iCreateAPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        adminHomePage = PageFactory.initElements(WebDrv.getInstance().getWebDriver(), AdminHomePage.class);
        adminHomePage.clickOnPages();
        adminHomePage.clickonAddChildPage();
        adminHomePage.clickonAddPersonPage();
        adminHomePage.addPageTitle("John Warwick Test");
        adminHomePage.clickonOptionMenu();
        adminHomePage.clickOnPublish();
        WebDrv.getInstance().getWebDriver().close();
    }

    @Then("^page shows up as searchable content$")
    public void pageShowsUpAsSearchableContent() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        homePage = PageFactory.initElements(WebDrv.getInstance().openBrowser("http://localhost:8000"), HomePagePOM.class);
        homePage.typeKeys("John Warwick");
        homePage.clickOnSearch();
        WebDrv.getInstance().getWebDriver().close();
    }
}
