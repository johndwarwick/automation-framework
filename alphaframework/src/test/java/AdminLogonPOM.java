import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminLogonPOM {

        @FindBy(css = "#id_username") private WebElement username;
        @FindBy(css = "#id_password") private WebElement password;
        @FindBy(css = "#wagtail > div > div > form > ul > li.submit > button") private WebElement signinButton;

        public AdminLogonPOM(){
        }

        public void enterUsername( String sequence ){
            username.sendKeys(sequence);
        }

        public void enterPassword(String sequence){
            password.sendKeys(sequence);
        }

        public void clickOnSignIn(){
            signinButton.click();
        }
}


