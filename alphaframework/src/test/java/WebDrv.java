import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDrv {
    private static WebDrv instance=null;
    private WebDriver webDriver;


    private WebDrv(){ }

    public WebDriver openBrowser(String url){

        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.navigate().to(url);
        return webDriver;
    }

    public WebDriver getWebDriver() {
        return webDriver;
    }

    public static WebDrv getInstance(){
        if(instance==null){
            instance = new WebDrv();
        }
        return instance;
    }

}
