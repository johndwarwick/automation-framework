import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePagePOM {

    @FindBy(css = "#query") private WebElement pheSearchEdit;
    @FindBy(css = "#main-content > form > button") private WebElement searchButton;

    public HomePagePOM(){
    }

    public void typeKeys( String sequence ){
        pheSearchEdit.sendKeys(sequence);
    }

    public void clickOnSearch(){
        searchButton.click();
    }

    public void closeBrowser(){
        WebDrv.getInstance().getWebDriver().close();
    }

}
