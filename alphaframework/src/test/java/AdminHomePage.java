import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminHomePage {

    @FindBy(css = "#wagtail > div > div.content-wrapper > div > section > ul > li.icon.icon-doc-empty-inverse > a > span") private WebElement pages;
    @FindBy(css = "#page-reorder-form > table > thead > tr.index > td.title > ul > li:nth-child(3) > a") private WebElement addChildpage;
    @FindBy(css = "#wagtail > div > div.content-wrapper > div > div.nice-padding > ul > li:nth-child(7) > div > div > a") private WebElement addPersonPage;
    @FindBy(css = "#id_title") private WebElement pageTitle;
    @FindBy(css = "#page-edit-form > footer > ul > li.actions > div > div") private WebElement optionMenu;
    @FindBy(css = "#page-edit-form > footer > ul > li.actions > div > ul > li:nth-child(1) > button") private WebElement publish;

    public AdminHomePage(){}

    public void clickOnPages(){
        pages.click();
    }

    public void clickonAddChildPage(){
        addChildpage.click();
    }

    public void clickonAddPersonPage(){
        addPersonPage.click();
    }

    public void addPageTitle(String str){
        pageTitle.sendKeys(str);
    }

    public void clickonOptionMenu(){
        optionMenu.click();
    }

    public void clickOnPublish(){
        publish.click();
    }
}
