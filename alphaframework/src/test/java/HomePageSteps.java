import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;

public class HomePageSteps {

    private HomePagePOM homePagePOM;

    @Given("^I navigate to the intranet home page$")
    public void iNavigateToTheIntranetHomePage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        homePagePOM = PageFactory.initElements(WebDrv.getInstance().openBrowser("http://localhost:8000"), HomePagePOM.class);
    }

    @When("^I search for \"([^\"]*)\"$")
    public void iSearchFor(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        homePagePOM.typeKeys(arg0);
        homePagePOM.clickOnSearch();
    }

    @Then("^No search results are returned$")
    public void noSearchResultsAreReturned() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        WebDrv.getInstance().getWebDriver().close();
    }
}
