Feature: Create Content
  Scenario: As an admin user I want to create content in wagtail

    Given I log on as admin user with username "wagtail" and password "wagtail"
    When I create a page
    Then page shows up as searchable content
