Feature: Home Page Search
  Scenario:As a user I would like to use the search functionality

    Given I navigate to the intranet home page
    When I search for "something"
    Then No search results are returned